
public class Binary {
    public static void main (String args[]){
        int temp = 0;
        int index = search(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9}, 5);
        System.out.println(5 + " " + index);
    }

    public static int search(int array[], int temp){

        int left = 0;
        int right = array.length -1;
        while(left <= right){
            int middle = (left + right) / 2;
            if(array[middle] == temp){
                return middle;
            }else if(array[middle] < temp){
                left = middle + 1;
            }else if(array[middle] > temp){
                right = middle - 1;

            }}return -1;}}
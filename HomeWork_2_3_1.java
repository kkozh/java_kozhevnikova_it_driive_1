public class HomeWork_2_3_1 {
    public static void sort(int array[]){
        for(int i = 0; i < array.length; i++){
            int min = array[i];
            int temp = i;
 
            for(int j = i + 1; j < array.length; j++){
                if(array[j] < min){
                    min = array[j];
                    temp = j;
                }
            }if(i != temp){
                int temporary = array[i];
                array[i] = array[temp];
                array[temp] = temporary;
            }
            System.out.println(array[i]);
        }}
        public static void main(String args[]){
            sort(new int[]{5, 34, 12, 57, 3, 8, 10});
 
        }
    }
import java.util.Scanner;

class HomeWork_2_2_2{

// x^2
public static double f(double x) {
return x * x;
}

public static double integralByRectangles(double a, double b, int n) {

double h = (b - a) / n;
double result1 = 0;
for (double x = a; x <= b; x = x + h) {
double currentRectangle = f(x) * h;
result1 = result1 + currentRectangle;
}

return result1;}


public static double integralBySimpson(double a, double b, int n) {
double h = (b - a) / n;

double result2 = 0;
for (double x = a; x <= b; x = x + 2 * h) {
double currentRectangle = f(x - h) + 4 * f(x) + f(x + h);
result2 = result2 + currentRectangle;
}

return result2 * (h / 3);
}

public static void main(String[] args) {
Scanner scanner = new Scanner(System.in);
double a = scanner.nextDouble();
double b = scanner.nextDouble();
int[] n = {100,1000,10000,100000};
for(int i = 0; i < n.length; i++){

double result1 = integralByRectangles(a, b, n[i]);

double result2 = integralBySimpson(a, b, n[i]);
System.out.println("Rectangles " + n[i] + " = " + result1);
System.out.println("Simpson " + n[i] + " = " + result2);
}



}}
import java.util.Scanner;
public class Three {
    public static double pow(double x, int ys){
 
        return Math.pow(x,ys);
    }
    public static double integralByRectangles(double a, double b, int y){
        double n = (b - a) / y;
        double result1 = 0;
        for(double x = 0; x <= b; x = x + n){
            double currentRectangle = x * n;
            result1 = result1 + currentRectangle;
        }return result1;
    }
 
 
    public static double integralBySimpson(double a, double b, int y){
        double n = (b - a) / y;
        double result2 = 0;
        for(double x = 0; x <= b; x = x + 2 * n){
            double currentRectangle = (x - n) + 4 * x + (x + n);
            result2 = result2 + currentRectangle;
        }return result2 * n / 3;
    }
 
 
    public static void main (String args[]){
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        int y[] = {100,1000,10000,100000}; //разбиения
        int ys[] = {2,3,4,5,6,7}; //степени
        for(int i = 0; i < y.length; i++){
            double result1 = integralByRectangles(a, b, y[i]);
            double result2 = integralBySimpson(a, b, y[i]);
 
 
            for(int z = 0; z < ys.length; z++){
                System.out.println("Rectangles " + y[i] + " - " + ys[z] +" = " + result1);
                System.out.println("Simpson " + y[i] + " - " + ys[z] +" = " + result2);
            }}
    }
}